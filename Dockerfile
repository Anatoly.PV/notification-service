FROM maven:3.8.1-jdk-8-slim AS build
COPY . /home/app
RUN mvn -f /home/app/pom.xml clean package -Dmaven.test.skip=true

FROM openjdk:8-jdk-alpine
WORKDIR /home/app
COPY --from=build /home/app/target/notification-service-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -jar /home/app/app.jar
