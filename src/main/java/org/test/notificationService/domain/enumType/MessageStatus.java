package org.test.notificationService.domain.enumType;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "MessageStatus", description = "Статус")
public enum MessageStatus {

    SUCCESS("Успешно"),
    READY("Готовность к отправке"),
    WAITING("В ожидании"),
    EXPIRED("Просрочен"),
    REJECTED("Отказано");

    private final String message;

    MessageStatus(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
