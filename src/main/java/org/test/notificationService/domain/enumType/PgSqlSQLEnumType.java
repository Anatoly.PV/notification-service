package org.test.notificationService.domain.enumType;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.EnumType;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;


/**
 * JavaBean that allows to convert java enum to PostgreSQL on entities
 * <p/>
 * @see MessageStatus
 */
public class PgSqlSQLEnumType extends EnumType<Enum<?>> {

    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {

        if (value == null) {

            st.setNull(index, Types.OTHER);

        } else {

            if (value instanceof MessageStatus) {
                st.setObject(index, value, Types.OTHER);
            }

        }
    }
}
