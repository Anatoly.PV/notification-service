package org.test.notificationService.domain.dto.client;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

/**
 * This class helped to collect data from input by client parameters
 */
@Data
@Builder
@Schema(name = "CreateClientDto", description = "Представление создания клиента")
public class ClientDto {

    @Schema(name = "phoneNumber", example = "7903904950342", description = "Номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)")
    private String phoneNumber;

    @Schema(name = "code", example = "903", description = "Код мобильного оператора")
    private String code;

    @Schema(name = "tag", example = "описание", description = "Тег (произвольная метка)")
    private String tag;

    @Schema(name = "timeZone", example = "Europe/Moscow", description = "Часовой пояс")
    private String timeZone;

}