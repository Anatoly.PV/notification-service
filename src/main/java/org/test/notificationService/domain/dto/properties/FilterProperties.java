package org.test.notificationService.domain.dto.properties;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "FilterProperties", description = "Представление отбора клиентов")
public class FilterProperties {

    @Schema(name = "phoneNumber", example = "7903904950342", description = "Текст сообщения для доставки клиенту")
    private String phoneNumber;

    @Schema(name = "code", example = "903", description = "Текст сообщения для доставки клиенту")
    private String code;

    @Schema(name = "tag", example = "описание", description = "Текст сообщения для доставки клиенту")
    private String tag;

    @Schema(name = "timeZone", example = "Europe/Moscow", description = "Текст сообщения для доставки клиенту")
    private String timeZone;
}