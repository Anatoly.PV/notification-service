package org.test.notificationService.domain.dto.message;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import org.springframework.web.client.RestTemplate;
import org.test.notificationService.domain.enumType.MessageStatus;

import java.util.Calendar;

/**
 * This class helped to collect from persistence date by message parameters
 */
@Data
@Builder
@Schema(name = "MessageInfoResultDto", description = "Представление возврата сообщения")
public class MessageInfoResultDto {

    @Schema(name = "id", example = "115646", description = "Уникальный идентификатор (ID) сообщения")
    private Integer id;

    @Schema(type="string" , example = "2022-01-31T00:00:00", description = "Дата и время создания сообщения")
    private Calendar createData;

    @Schema(type="string" , example = "2022-01-31T00:00:00", description = "Дата и время отправки сообщения")
    private Calendar sendDate;

    @Schema(description = "Статус")
    private MessageStatus checkStatus;

    @Schema(name = "content", example = "тест", description = "Текст сообщения для доставки клиенту")
    private String content;

    @Schema(name = "phoneNumber", example = "7903904950342", description = "Номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)")
    private String phoneNumber;

}
