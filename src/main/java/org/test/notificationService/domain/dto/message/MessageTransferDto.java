package org.test.notificationService.domain.dto.message;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.test.notificationService.domain.dto.client.ClientDto;
import org.test.notificationService.domain.dto.mailing.MailingDto;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.service.router.SchedulerMessageRouter;

import java.util.Date;
import java.util.UUID;

/**
 * This class helped to collect data from mailing and clients
 * It used inside camel Scheduler {@link SchedulerMessageRouter}
 * For understood the purpose of parameters see:
 *
 * @see ClientDto
 * @see MailingDto
 */
@Data
@SuperBuilder
public class MessageTransferDto {

    private Integer id;

    private Date createData;

    private Date sendDate;

    private MessageStatus status;

    private UUID mailingId;

    private UUID clientId;

    private Date startDate;

    private Date endDate;

}
