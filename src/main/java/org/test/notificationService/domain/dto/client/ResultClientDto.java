package org.test.notificationService.domain.dto.client;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.UUID;

/**
 * This class helped to collect from persistence date by client parameters
 */
@Data
@Schema(name = "ResultClientDto", description = "Представление возврата клиента")
public class ResultClientDto {

    @Schema(name = "id", example = "90692ca1-e2fa-44b5-a172-3f1719ce8481", description = "Уникальный идентификатор (ID) клиента")
    private UUID id;

    @Schema(name = "phoneNumber", example = "7903904950342", description = "Номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)")
    private String phoneNumber;

    @Schema(name = "code", example = "903", description = "Код мобильного оператора")
    private String code;

    @Schema(name = "tag", example = "описание", description = "Тег (произвольная метка)")
    private String tag;

    @Schema(name = "timeZone", example = "Europe/Moscow", description = "Часовой пояс")
    private String timeZone;

}