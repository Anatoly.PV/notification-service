package org.test.notificationService.domain.dto.mailing;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.test.notificationService.domain.dto.client.ResultClientDto;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

/**
 * This class helped to collect from persistence date by mailing parameters
 */
@Data
@Schema(name = "ResultMailingDto", description = "Представление возврата рассылки")
public class ResultMailingDto {

    @Schema(name = "id", example = "90692ca1-e2fa-44b5-a172-3f1719ce8481", description = "Уникальный идентификатор (ID) рассылки")
    private UUID id;

    @Schema(type="string" , example = "2022-01-31T00:00:00", description = "Дата и время запуска рассылки")
    private Calendar startDate;

    @Schema(name = "content", example = "тест", description = "Текст сообщения для доставки клиенту")
    private String content;

    private List<ResultClientDto> clients;

    @Schema(type="string", example = "2022-01-31T00:00:00", description = "Дата и время окончания рассылки")
    private Calendar endData;

}
