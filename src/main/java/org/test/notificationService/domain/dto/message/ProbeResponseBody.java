package org.test.notificationService.domain.dto.message;

import lombok.Data;
import org.springframework.web.client.RestTemplate;

/**
 * This class helped to collect data from input by message parameters
 * It used inside RestTemplate{@link RestTemplate } for send to prod service
 * <p/>
 * code - error code
 * <p/>
 * message - information
 */
@Data
public class ProbeResponseBody {

    private int code;

    private String message;

}
