package org.test.notificationService.domain.dto.message;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.test.notificationService.domain.enumType.MessageStatus;

import java.util.Calendar;
import java.util.UUID;

@Data
@Schema(name = "MessageMailingDetails", description = "Детализация сообщения")
public class MessageMailingDetails {

    @Schema(name = "id", example = "115646", description = "Уникальный идентификатор (ID) сообщения")
    private Integer id;

    @Schema(name = "mailing", example = "90692ca1-e2fa-44b5-a172-3f1719ce8481", description = "Уникальный идентификатор (ID) рассылки")
    private UUID mailing;

    @Schema(type="string" , example = "2022-01-31T00:00:00", description = "Дата и время создания сообщения")
    private Calendar createData;

    @Schema(type="string" , example = "2022-01-31T00:00:00", description = "Дата и время отправки сообщения")
    private Calendar sendDate;

    @Schema(description = "Статус")
    private MessageStatus checkStatus;

    @Schema(name = "content", example = "тест", description = "Текст сообщения для доставки клиенту")
    private String content;

    @Schema(name = "phoneNumber", example = "7903904950342", description = "Номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)")
    private String phoneNumber;

    @Schema(name = "timeZone", example = "Europe/Moscow", description = "Часовой пояс")
    private String timeZone;

}
