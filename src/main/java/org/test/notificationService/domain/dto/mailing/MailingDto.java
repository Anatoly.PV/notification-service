package org.test.notificationService.domain.dto.mailing;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import org.test.notificationService.domain.dto.properties.FilterProperties;

import java.util.Calendar;

/**
 * This class helped to collect data from input by mailing parameters
 */
@Data
@Builder
@Schema(name = "MailingDto", description = "Представление создания рассылки")
public class MailingDto {

    @Schema(type="string" , example = "2022-01-31T00:00:00", description = "Дата и время запуска рассылки")
    private Calendar startDate;

    @Schema(name = "content", example = "тест", description = "Текст сообщения для доставки клиенту")
    private String content;

    private FilterProperties filterProperties;

    @Schema(type="string", example = "2022-01-31T00:00:00", description = "Дата и время окончания рассылки")
    private Calendar endData;

}