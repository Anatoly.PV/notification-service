package org.test.notificationService.domain.dto.message;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.client.RestTemplate;

/**
 * This class helped to collect data from input by message parameters
 * It used inside RestTemplate{@link RestTemplate } for send to prod service
 * <p/>
 * id - message identifier
 * <p/>
 * phone - phone number on format 7XXXXXXXXX
 * <p/>
 * text - message content
 */
@Data
@Builder
public class MessageDto {

    private int id;

    private Long phone;

    private String text;

}
