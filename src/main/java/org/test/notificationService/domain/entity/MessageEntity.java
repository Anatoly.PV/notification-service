package org.test.notificationService.domain.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.domain.enumType.PgSqlSQLEnumType;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "message")
@Table(name = "message")
@TypeDef(name = "pgsql_enum", typeClass = PgSqlSQLEnumType.class)
@SuperBuilder
public class MessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "create_date")
    private Calendar createData;

    @Column(name = "send_date")
    private Calendar sendDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "message_status")
    @Type(type = "pgsql_enum")
    private MessageStatus checkStatus;

    @Column(name = "delete_flag")
    private Boolean deleteFlag = false;

    @ManyToOne
    @JoinColumn(name = "client_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private ClientEntity client;

    @ManyToOne
    @JoinColumn(name = "mailing_id")
    private MailingEntity mailing;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageEntity that = (MessageEntity) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getCreateData(), that.getCreateData()) && Objects.equals(getSendDate(), that.getSendDate()) && getCheckStatus() == that.getCheckStatus() && Objects.equals(getDeleteFlag(), that.getDeleteFlag());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCreateData(), getSendDate(), getCheckStatus(), getDeleteFlag());
    }
}