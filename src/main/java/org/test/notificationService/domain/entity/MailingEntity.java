package org.test.notificationService.domain.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "mailing")
@Table(name = "mailing")
public class MailingEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", columnDefinition = "uuid")
    @ColumnDefault("gen_random_uuid()")
    private UUID id;

    @Column(name = "content")
    private String content;

    @Column(name = "start_date")
    private Calendar startDate;

    @Column(name = "end_date")
    private Calendar endData;

    @Column(name = "delete_flag")
    private Boolean deleteFlag = false;

    @OneToMany(mappedBy = "mailing", fetch = FetchType.EAGER)
    private Set<MessageEntity> message = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "mailing_to_client",
            joinColumns = {@JoinColumn(name = "mailing_id")},
            inverseJoinColumns = {@JoinColumn(name = "client_id")})
    @ToString.Exclude
    private Set<ClientEntity> clientEntities = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailingEntity entity = (MailingEntity) o;
        return Objects.equals(getId(), entity.getId()) && Objects.equals(getContent(), entity.getContent()) && Objects.equals(getStartDate(), entity.getStartDate()) && Objects.equals(getEndData(), entity.getEndData()) && Objects.equals(getDeleteFlag(), entity.getDeleteFlag());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getContent(), getStartDate(), getEndData(), getDeleteFlag());
    }
}