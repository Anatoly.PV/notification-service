package org.test.notificationService.domain.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "client")
@Table(name = "client")
public class ClientEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", columnDefinition = "uuid")
    @ColumnDefault("gen_random_uuid()")
    private UUID id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "code")
    private String code;

    @Column(name = "tag")
    private String tag;

    @Column(name = "time_zone")
    private String timeZone;

    @Column(name = "delete_flag")
    private Boolean deleteFlag = false;

    @ManyToMany(mappedBy = "clientEntities", fetch = FetchType.EAGER)
    private Set<MailingEntity> mailingEntity = new HashSet<>();

    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    private Set<MessageEntity> message = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity entity = (ClientEntity) o;
        return Objects.equals(getId(), entity.getId()) && Objects.equals(getPhoneNumber(), entity.getPhoneNumber()) && Objects.equals(getCode(), entity.getCode()) && Objects.equals(getTag(), entity.getTag()) && Objects.equals(getTimeZone(), entity.getTimeZone()) && Objects.equals(getDeleteFlag(), entity.getDeleteFlag());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPhoneNumber(), getCode(), getTag(), getTimeZone(), getDeleteFlag());
    }
}