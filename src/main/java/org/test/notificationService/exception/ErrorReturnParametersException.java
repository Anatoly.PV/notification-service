package org.test.notificationService.exception;

/**
 * This class helped define error by async invoker
 */
public class ErrorReturnParametersException extends RuntimeException {

    public ErrorReturnParametersException(String order) {
        super(order);
    }
}