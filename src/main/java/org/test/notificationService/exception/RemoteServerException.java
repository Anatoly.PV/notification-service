package org.test.notificationService.exception;

/**
 * This class helped define error by integration service
 */
public class RemoteServerException extends RuntimeException{

    public RemoteServerException(String order) {
        super(order);
    }
}
