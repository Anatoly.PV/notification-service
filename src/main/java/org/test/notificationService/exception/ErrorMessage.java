package org.test.notificationService.exception;

/**
 * This class helped define error messages
 */
public class ErrorMessage {

    public static final String NOT_FOUND_CLIENT = "Клиент не найден";

    public static final String NOT_FOUND_MESSAGE = "Сообщение не найдено";

    public static final String NOT_FOUND_MAILING = "Рассылка не найдена";

    public static final String REMOTE_ERROR = "Сервер обработки сообщений вернул ошибку";
}
