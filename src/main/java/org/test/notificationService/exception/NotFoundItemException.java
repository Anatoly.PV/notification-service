package org.test.notificationService.exception;

/**
 * This class helped define error with 404 status
 */
public class NotFoundItemException extends RuntimeException{

    public NotFoundItemException(String order) {
        super(order);
    }
}
