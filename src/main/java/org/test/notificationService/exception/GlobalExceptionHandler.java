package org.test.notificationService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

/**
 * This class helped define exception and set statuses
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundItemException.class)
    public ResponseEntity<ErrorResponse> handleError(NotFoundItemException exception) {
        ErrorResponse message = ErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .status(HttpStatus.NOT_FOUND.value())
                .error(HttpStatus.NOT_FOUND.getReasonPhrase())
                .message(exception.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }

    @ExceptionHandler(RemoteServerException.class)
    public ResponseEntity<ErrorResponse> handleError(RemoteServerException exception) {
        ErrorResponse message = ErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .status(HttpStatus.GATEWAY_TIMEOUT.value())
                .error(HttpStatus.GATEWAY_TIMEOUT.getReasonPhrase())
                .message(exception.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.GATEWAY_TIMEOUT).body(message);
    }

    @ExceptionHandler(ErrorReturnParametersException.class)
    public ResponseEntity<ErrorResponse> handleError(ErrorReturnParametersException exception) {
        ErrorResponse message = ErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .status(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .error(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase())
                .message(exception.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(message);
    }
}