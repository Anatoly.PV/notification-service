package org.test.notificationService.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.notificationService.domain.dto.mailing.MailingDto;
import org.test.notificationService.domain.dto.mailing.ResultMailingDto;
import org.test.notificationService.domain.dto.message.MessageMailingDetails;
import org.test.notificationService.domain.entity.ClientEntity;
import org.test.notificationService.domain.entity.MailingEntity;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.exception.NotFoundItemException;
import org.test.notificationService.repository.mailing.MailingRepository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.test.notificationService.exception.ErrorMessage.NOT_FOUND_MAILING;

/**
 * This class service include methods for processed
 * data by mailing business
 *
 * @see ClientService
 * @see MailingRepository
 * @see MessageService
 */
@Service
public class MailingService {

    @Resource(name = "modelMapper")
    private ModelMapper modelMapper;

    @Resource
    private MailingRepository mailingRepository;

    @Resource
    private ClientService clientService;

    @Resource
    private MessageService messageService;

    /**
     * Create mailing method
     *
     * @param createMailingData {@link MailingDto}
     * @return saved result {@link ResultMailingDto}
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultMailingDto create(MailingDto createMailingData) {

        MailingEntity entity = modelMapper.map(createMailingData, MailingEntity.class);

        mailingRepository.save(entity);

        Set<ClientEntity> clientEntities = clientService.setRelationMailingToClients(createMailingData.getFilterProperties());

        entity.getClientEntities().addAll(clientEntities);

        messageService.generateMessages(entity);

        return modelMapper.map(entity, ResultMailingDto.class);

    }

    /**
     * Get mailing method
     *
     * @param id {@link UUID}
     * @return get result {@link ResultMailingDto}
     */
    @Transactional
    public ResultMailingDto getById(UUID id) {

        return modelMapper.map(mailingRepository.getById(id), ResultMailingDto.class);

    }

    /**
     * Soft delete mailing method, don't delete from data base
     *
     * @param id {@link UUID}
     * @return updated result {@link Boolean}
     */
    @Transactional
    public boolean softDelete(UUID id) {

        MailingEntity entity = mailingRepository.findById(id).filter(message -> !message.getDeleteFlag())
                .orElseThrow(() -> new NotFoundItemException(NOT_FOUND_MAILING));

        entity.setDeleteFlag(true);

        return entity.getDeleteFlag();

    }

    /**
     * filter by id and status
     *
     * @param id {@link UUID}
     * @return filtered result {@link MessageMailingDetails}
     */
    @Transactional
    public List<MessageMailingDetails> getAllByStatus(UUID id, MessageStatus status) {

        MailingEntity entity = mailingRepository.getById(id);

        return messageService.getByMailing(entity,status);
    }

    /**
     * Get mailing method
     *
     * @return get result {@link ResultMailingDto}
     */
    @Transactional
    public List<ResultMailingDto> getAll() {

        return mailingRepository.findAll().stream()
                .map(mailingEntity -> modelMapper
                        .map(mailingEntity, ResultMailingDto.class)).collect(Collectors.toList());
    }

    /**
     * Update mailing method
     *
     * @param id {@link UUID}
     * @param updateRequestData {@link MailingDto}
     * @return updated result {@link ResultMailingDto}
     */
    @Transactional
    public ResultMailingDto update(UUID id, MailingDto updateRequestData) {

        MailingEntity entity = mailingRepository.findById(id).orElseThrow(() -> new NotFoundItemException(NOT_FOUND_MAILING));

        modelMapper.map(updateRequestData, entity);

        return modelMapper.map(entity, ResultMailingDto.class);
    }
}