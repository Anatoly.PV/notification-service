package org.test.notificationService.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.test.notificationService.domain.dto.client.ClientDto;
import org.test.notificationService.domain.dto.client.ResultClientDto;
import org.test.notificationService.domain.dto.mailing.ResultMailingDto;
import org.test.notificationService.domain.dto.message.MessageInfoResultDto;
import org.test.notificationService.domain.dto.message.MessageMailingDetails;
import org.test.notificationService.domain.entity.MailingEntity;
import org.test.notificationService.domain.entity.MessageEntity;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.exception.NotFoundItemException;
import org.test.notificationService.repository.message.MessageRepository;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.test.notificationService.exception.ErrorMessage.NOT_FOUND_MESSAGE;

/**
 * This class service include methods for processed
 * data by message business
 *
 * @see MessageRepository
 */
@Service
public class MessageService {

    @Resource(name = "modelMapper")
    private ModelMapper modelMapper;

    @Resource
    private MessageRepository messageRepository;

    /**
     * Get message method
     *
     * @return get result {@link MessageInfoResultDto}
     */
    @Transactional
    public List<MessageInfoResultDto> getAll() {

        List<MessageEntity> entities = messageRepository.getAll();

        return entities.stream()
                .map(entity -> modelMapper.map(entity, MessageInfoResultDto.class))
                .collect(Collectors.toList());

    }

    /**
     * Create message method if it can to send
     *
     * @param mailing {@link MailingEntity}
     */
    @Transactional
    public void generateMessages(MailingEntity mailing) {

        mailing.getClientEntities().forEach(clientEntity -> {

            MessageEntity messageEntity = MessageEntity.builder()
                    .createData(Calendar.getInstance())
                    .checkStatus(MessageStatus.READY)
                    .client(clientEntity)
                    .deleteFlag(false)
                    .mailing(mailing)
                    .build();

            messageRepository.save(messageEntity);
        });
    }

    /**
     * Get message method
     *
     * @param id {@link Integer}
     * @return get result {@link MessageEntity}
     */
    @Transactional
    public MessageEntity getById(Integer id) {

        return messageRepository.getById(id);

    }

    /**
     * If there was an attempt to send this method will be called
     *
     * @param entity {@link MessageEntity}
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveMessageResult(MessageEntity entity) {
        entity.setSendDate(Calendar.getInstance());
        messageRepository.save(entity);
    }

    /**
     * If there is no possibility of sending this method will be called
     *
     * @param id {@link Integer}
     */
    @Transactional
    public void expire(Integer id) {

        MessageEntity entity = messageRepository.findById(id).filter(message -> !message.getDeleteFlag())
                .orElseThrow(() -> new NotFoundItemException(NOT_FOUND_MESSAGE));

        entity.setCheckStatus(MessageStatus.EXPIRED);

    }

    /**
     * collect for filter by mailing and status
     *
     * @param mailing {@link MailingEntity}
     * @param status {@link MessageStatus}
     * @return filtered result {@link MessageMailingDetails}
     */
    @Transactional
    public List<MessageMailingDetails> getByMailing(MailingEntity mailing, MessageStatus status) {

        List<MessageEntity> allByMailing = messageRepository.findAllByMailingAndCheckStatus(mailing, status);

        return allByMailing.stream()
                .map(messageEntity -> modelMapper
                        .map(messageEntity, MessageMailingDetails.class)).collect(Collectors.toList());

    }
}