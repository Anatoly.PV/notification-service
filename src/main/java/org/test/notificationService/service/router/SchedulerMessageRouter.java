package org.test.notificationService.service.router;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.service.camel.MessageCheckout;
import org.test.notificationService.service.camel.MessageDateInspector;
import org.test.notificationService.service.camel.MessageExecutorService;
import org.test.notificationService.service.camel.MessageStorage;

import javax.annotation.Resource;

import static org.test.notificationService.constant.CamelHelper.*;

/**
 * Core of processing messages, include routers which respond for a specific task
 * routByTimer - scheduler with timeout
 * routByExecutor - rest executor
 * routByCheckout - service helper for expiration
 * routByStorage - service helper for save
 *
 * @see Processor
 */
@Service
public class SchedulerMessageRouter extends RouteBuilder {

    @Resource
    private MessageExecutorService executor;

    @Resource
    private MessageDateInspector inspector;

    @Resource
    private MessageCheckout checkout;

    @Resource
    private MessageStorage storage;

    @Value("${camel.routByTimer}")
    private String routByTimer;

    @Value("${camel.routByDateSource}")
    private String routByDateSource;

    @Value("${camel.routByExecutor}")
    private String routByExecutor;

    @Value("${camel.routByCheckout}")
    private String routByCheckout;

    @Value("${camel.routByStorage}")
    private String routByStorage;

    @Override
    public void configure() {

        from(routByTimer)
                .process(exchange -> exchange.getIn().setBody(SQL_QUERY))
                .to(routByDateSource)
                .log(body().toString())
                .split()
                    .body()
                    .process(inspector)
                    .log(body().toString())
                    .choice()
                        .when(header(DESTINATION).isEqualTo(MessageStatus.READY))
                        .to(routByExecutor)
                        .when(header(DESTINATION).isEqualTo(MessageStatus.EXPIRED))
                        .to(routByCheckout)
                        .when(header(DESTINATION).isEqualTo(MessageStatus.REJECTED))
                        .to(routByExecutor)
                        .when(header(DESTINATION).isEqualTo(MessageStatus.WAITING))
                        .to(routByStorage)
                .endChoice()
                .end();

        from(routByExecutor)
                .process(executor)
                .log(body().toString() + IS_EXECUTED);

        from(routByCheckout)
                .process(checkout)
                .log(body().toString() + IS_CHECKOUT);

        from(routByStorage)
                .process(storage)
                .log(body().toString() + IS_SAVED);

    }
}