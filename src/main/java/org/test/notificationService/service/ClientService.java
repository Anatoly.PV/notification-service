package org.test.notificationService.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.notificationService.domain.dto.client.ClientDto;
import org.test.notificationService.domain.dto.client.ResultClientDto;
import org.test.notificationService.domain.dto.properties.FilterProperties;
import org.test.notificationService.domain.entity.ClientEntity;
import org.test.notificationService.exception.ErrorReturnParametersException;
import org.test.notificationService.exception.NotFoundItemException;
import org.test.notificationService.repository.client.ClientRepository;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.test.notificationService.exception.ErrorMessage.NOT_FOUND_CLIENT;

/**
 * This class service include methods for processed
 * data by client business
 *
 * @see ClientRepository
 */
@Service
public class ClientService {

    @Resource(name = "modelMapper")
    private ModelMapper modelMapper;

    @Resource
    private ClientRepository clientRepository;

    /**
     * Create client method
     *
     * @param createInputData {@link ClientDto}
     * @return saved result {@link ResultClientDto}
     */
    @Transactional
    public ResultClientDto create(ClientDto createInputData) {

        ClientEntity entity = modelMapper.map(createInputData, ClientEntity.class);

        clientRepository.save(entity);

        return modelMapper.map(entity, ResultClientDto.class);

    }

    /**
     * Update client method
     *
     * @param id {@link UUID}
     * @param updateRequestData {@link ClientDto}
     * @return updated result {@link ResultClientDto}
     */
    @Transactional
    public ResultClientDto update(UUID id, ClientDto updateRequestData) {

        ClientEntity entity = clientRepository.findById(id).orElseThrow(() -> new NotFoundItemException(NOT_FOUND_CLIENT));

        modelMapper.map(updateRequestData, entity);

        return modelMapper.map(entity, ResultClientDto.class);

    }

    /**
     * Soft delete client method, don't delete from data base
     *
     * @param id {@link UUID}
     * @return updated result {@link Boolean}
     */
    @Transactional
    public Boolean softDelete(UUID id) {

        ClientEntity entity = clientRepository.findById(id).filter(clientEntity -> !clientEntity.getDeleteFlag())
                .orElseThrow(() -> new NotFoundItemException(NOT_FOUND_CLIENT));

        entity.setDeleteFlag(true);

        return entity.getDeleteFlag();

    }

    /**
     * Setting up client relations and mailing lists, asynchronous request is shown as an example
     *
     * @param properties {@link FilterProperties}
     * @return updated result {@link ClientEntity}
     */
    @Transactional
    public Set<ClientEntity> setRelationMailingToClients(FilterProperties properties) {

        List<CompletableFuture<List<ClientEntity>>> futures = new ArrayList<>();
        Set<ClientEntity> allClients = new HashSet<>();

        futures.add(clientRepository.getByPhoneNumberAsync(properties.getPhoneNumber()));
        futures.add(clientRepository.getByCodeAsync(properties.getCode()));
        futures.add(clientRepository.getByTimeZoneAsync(properties.getTimeZone()));
        futures.add(clientRepository.getByTagAsync(properties.getTimeZone()));

        futures.forEach(CompletableFuture::join);

        for (CompletableFuture<List<ClientEntity>> future : futures) {
            try {
                List<ClientEntity> clientEntities = future.get();
                allClients.addAll(clientEntities);
            } catch (InterruptedException | ExecutionException e) {
                throw new ErrorReturnParametersException("Получены неподходящие параметры");
            }
        }

        if (allClients.size() == 0) {
            throw new NotFoundItemException("Клиенты не найдены");
        }
        return allClients;
    }

    /**
     * Get clients method
     *
     * @return get result {@link ResultClientDto}
     */
    @Transactional
    public List<ResultClientDto> getAll() {

        return clientRepository.findAll().stream()
                .map(clientEntity -> modelMapper
                        .map(clientEntity, ResultClientDto.class)).collect(Collectors.toList());

    }
}