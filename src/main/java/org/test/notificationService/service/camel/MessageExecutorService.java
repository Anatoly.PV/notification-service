package org.test.notificationService.service.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.test.notificationService.domain.dto.message.MessageDto;
import org.test.notificationService.domain.dto.message.MessageTransferDto;
import org.test.notificationService.domain.dto.message.ProbeResponseBody;
import org.test.notificationService.domain.entity.MessageEntity;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.exception.RemoteServerException;
import org.test.notificationService.service.MessageService;

import javax.annotation.Resource;
import java.util.Objects;

import static org.test.notificationService.exception.ErrorMessage.REMOTE_ERROR;

/**
 * Camel processor helped send message
 *
 * @see Processor
 */
@Service
public class MessageExecutorService implements Processor {

    @Value("${probe.service.url}")
    private String serviceUrl;

    @Resource(name = "restTemplate")
    private RestTemplate restTemplate;

    @Resource
    private MessageService messageService;

    @Resource
    private HttpHeaders headers;

    @Override
    @Transactional(rollbackFor = RemoteServerException.class, propagation = Propagation.REQUIRED)
    public void process(Exchange exchange) {

        if (exchange.getIn().getBody() instanceof MessageTransferDto) {

            MessageTransferDto messageTransferDto = (MessageTransferDto) exchange.getIn().getBody();

            MessageEntity entity = messageService.getById(messageTransferDto.getId());

            entity.setCheckStatus(MessageStatus.WAITING);

            messageService.saveMessageResult(entity);

            try {
                MessageDto messageDto = MessageDto.builder()
                        .id(entity.getId())
                        .phone(Long.parseLong(entity.getClient().getPhoneNumber()))
                        .text(entity.getMailing().getContent())
                        .build();

                UriComponentsBuilder resourceUrl = UriComponentsBuilder.fromUriString(serviceUrl + messageDto.getId());

                ResponseEntity<ProbeResponseBody> result = restTemplate.exchange(
                        resourceUrl.toUriString(),
                        HttpMethod.POST,
                        new HttpEntity<>(messageDto, headers),
                        ProbeResponseBody.class
                );

                entity.setCheckStatus(Objects.requireNonNull(result.getBody()).getCode() == 0 ? MessageStatus.SUCCESS : MessageStatus.REJECTED);

                messageService.saveMessageResult(entity);

            } catch (Exception ex) {
                throw new RemoteServerException(REMOTE_ERROR);
            }
        }
    }
}