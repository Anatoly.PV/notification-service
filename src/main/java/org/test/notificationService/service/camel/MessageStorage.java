package org.test.notificationService.service.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.test.notificationService.domain.dto.message.MessageTransferDto;
import org.test.notificationService.domain.entity.MessageEntity;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.service.MessageService;

import javax.annotation.Resource;

/**
 * Camel processor helped save the message for the next dispatch
 *
 * @see Processor
 */
@Service
public class MessageStorage implements Processor {

    @Resource
    private MessageService messageService;

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void process(Exchange exchange) {

        if (exchange.getIn().getBody() instanceof MessageTransferDto) {

            MessageTransferDto messageTransferDto = (MessageTransferDto) exchange.getIn().getBody();

            MessageEntity entity = messageService.getById(messageTransferDto.getId());

            entity.setCheckStatus(MessageStatus.WAITING);

            messageService.saveMessageResult(entity);

        }

    }
}

