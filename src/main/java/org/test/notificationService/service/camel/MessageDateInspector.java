package org.test.notificationService.service.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Service;
import org.test.notificationService.domain.dto.message.MessageTransferDto;
import org.test.notificationService.domain.enumType.MessageStatus;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Camel processor helped validate date interval for message
 *
 * @see Processor
 */
@Service
public class MessageDateInspector implements Processor {

    @Override
    public void process(Exchange exchange) {

        Map<?,?> body = exchange.getIn().getBody(LinkedHashMap.class);

        MessageTransferDto transferMessage = MessageTransferDto.builder()
                .id((Integer) body.get("id"))
                .createData((Date) body.get("create_date"))
                .sendDate((Date) body.get("send_date"))
                .status(MessageStatus.valueOf(body.get("message_status").toString()))
                .mailingId((UUID) body.get("mailing_id"))
                .clientId((UUID) body.get("client_id"))
                .startDate((Date) body.get("start_date"))
                .endDate((Date) body.get("end_date"))
                .build();

        MessageStatus headerControl = transferMessage.getStatus();
        if (headerControl == MessageStatus.EXPIRED) {
            return;
        }

        if ((new Date()).after(transferMessage.getStartDate())) {
            headerControl = MessageStatus.READY;
        }

        if ((new Date()).before(transferMessage.getStartDate())) {
            headerControl = MessageStatus.WAITING;
        }

        if ((new Date()).after(transferMessage.getEndDate())) {
            headerControl = MessageStatus.EXPIRED;
        }

        exchange.getIn().setBody(transferMessage);
        exchange.getIn().setHeader("destination", headerControl.toString());

    }

}
