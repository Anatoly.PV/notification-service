package org.test.notificationService.service.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Service;
import org.test.notificationService.domain.dto.message.MessageTransferDto;
import org.test.notificationService.domain.entity.MessageEntity;
import org.test.notificationService.service.MessageService;

import javax.annotation.Resource;

/**
 * Camel processor helped set expiration status for message
 *
 * @see Processor
 */
@Service
public class MessageCheckout implements Processor {

    @Resource
    private MessageService messageService;

    @Override
    public void process(Exchange exchange) {

        if (exchange.getIn().getBody() instanceof MessageTransferDto) {

            MessageTransferDto messageTransferDto = (MessageTransferDto) exchange.getIn().getBody();

            MessageEntity entity = messageService.getById(messageTransferDto.getId());

            messageService.expire(entity.getId());

        }

    }
}
