package org.test.notificationService.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.test.notificationService.domain.enumType.MessageStatus;

import java.util.ArrayList;

/**
 * Configured Swagger Document flow
 * <p/>
 * @see OpenAPI
 */
@Configuration
public class SwaggerConfiguration {

    @Value("${swagger.domain.name}")
    private String domainName;

    @Value("${swagger.domain.port}")
    private String port;

    /**
     * Base configurations for Swagger Ui, must include
     * domainName and port;
     *
     * @return Create OpenAPI configuration
     */
    @Bean
    public OpenAPI springShopOpenAPI() {

        ArrayList<Server> servers = new ArrayList<>();
        Server server = new Server();
        server.setUrl(domainName + ":" + port);
        servers.add(server);

        return new OpenAPI()
                .info(new Info().description("Сервис уведомлений")
                        .title("API")).servers(servers);
    }

    /**
     * Can be change controllers group name, and create target to scan
     *
     * @return Create GroupedOpenApi configuration
     */
    @Bean
    public GroupedOpenApi groupOpenApiV1() {
        String[] paths = {"/**"};
        String[] packagesToScan = {"org.test.notificationService.controller"};
        return GroupedOpenApi.builder().group("v1").pathsToMatch(paths).packagesToScan(packagesToScan).pathsToMatch("/", "/**").build();
    }
}