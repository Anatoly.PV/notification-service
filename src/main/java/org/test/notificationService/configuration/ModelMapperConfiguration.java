package org.test.notificationService.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.test.notificationService.domain.dto.mailing.ResultMailingDto;
import org.test.notificationService.domain.dto.message.MessageInfoResultDto;
import org.test.notificationService.domain.dto.message.MessageMailingDetails;
import org.test.notificationService.domain.entity.MailingEntity;
import org.test.notificationService.domain.entity.MessageEntity;

/**
 * JavaBean that allows for configuring a mapper context in bean style:
 * setting deep mapping entities and dto
 */
@Configuration
public class ModelMapperConfiguration {

    /**
     * Instantiate an instance of the given class, and using to map types
     * with deep mapping
     *
     * @return Created instance of a {@link ModelMapper} class
     */
    @Bean("modelMapper")
    public ModelMapper createModelMapper() {

        ModelMapper modelMapper = new ModelMapper();

        modelMapper.typeMap(MailingEntity.class, ResultMailingDto.class)
                .addMapping(MailingEntity::getClientEntities, ResultMailingDto::setClients);

        modelMapper.typeMap(MessageEntity.class, MessageInfoResultDto.class)
                .addMappings(mapper -> mapper.map(src -> src.getClient().getPhoneNumber(), MessageInfoResultDto::setPhoneNumber))
                .addMappings(mapper -> mapper.map(src -> src.getMailing().getContent(), MessageInfoResultDto::setContent));

        modelMapper.typeMap(MessageEntity.class, MessageMailingDetails.class)
                .addMappings(mapper -> mapper.map(src -> src.getMailing().getId(), MessageMailingDetails::setMailing))
                .addMappings(mapper -> mapper.map(src -> src.getMailing().getContent(), MessageMailingDetails::setContent))
                .addMappings(mapper -> mapper.map(src -> src.getClient().getPhoneNumber(), MessageMailingDetails::setPhoneNumber))
                .addMappings(mapper -> mapper.map(src -> src.getClient().getTimeZone(), MessageMailingDetails::setTimeZone));

        return modelMapper;

    }
}