package org.test.notificationService.configuration;

import org.springframework.core.convert.converter.Converter;
import org.test.notificationService.domain.enumType.MessageStatus;

/**
 * JavaBean that allows to convert java enum to PostgreSQL
 * <p/>
 * @see MessageStatus
 */
public class StringToEnumConverter implements Converter<String, MessageStatus> {

    /**
     * Implemented converter
     * <p/>
     * @return Create convert from {@link MessageStatus}  on PostgreSQL type
     */
    @Override
    public MessageStatus convert(String source) {
        return MessageStatus.valueOf(source.toUpperCase());
    }
}
