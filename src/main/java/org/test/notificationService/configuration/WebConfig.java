package org.test.notificationService.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * Spring Bean for convert enum to PostgreSQL
 * setting auth integration, headers.
 *
 * @see StringToEnumConverter
 * @see FormatterRegistry
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    /**
     * Adds a Formatter to format fields of the given type.
     *<p/>
     * @param registry the field type to format
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToEnumConverter());
    }

}
