package org.test.notificationService.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.client.RestTemplate;
import org.test.notificationService.service.router.SchedulerMessageRouter;

import javax.sql.DataSource;

/**
 * JavaBean that allows for configuring a Camel context in bean style:
 * setting data source configuration only
 *
 * @see SchedulerMessageRouter
 * @see DataSource
 */
@Configuration
public class CamelConfiguration {

    @Bean("restTemplate")
    public RestTemplate createRestTemplate() {
        return new RestTemplate();
    }

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driver;

    @Bean("myDataSource")
    @Primary
    public DataSource setupDataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setUrl(this.url);
        driverManagerDataSource.setUsername(this.username);
        driverManagerDataSource.setPassword(this.password);
        driverManagerDataSource.setDriverClassName(this.driver);
        return driverManagerDataSource;
    }
}
