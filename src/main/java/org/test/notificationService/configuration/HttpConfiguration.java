package org.test.notificationService.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

/**
 * Configuring a Rest template context in bean style:
 * setting auth integration, headers.
 *
 * @see RestTemplate
 * @see HttpHeaders
 */
@Configuration
public class HttpConfiguration {

    @Value("${probe.fbrq.cloud.jwt}")
    private String jwt;

    @Bean
    public RestTemplate createRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public HttpHeaders createHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization","Bearer " + jwt);
        return headers;
    }
}
