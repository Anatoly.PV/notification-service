package org.test.notificationService.asyncConfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Enables Spring's asynchronous method execution capability,
 * similar to functionality found in Spring's tasks
 * <p>
 * To be used together with @Configuration classes as follows,
 * enabling annotation-driven async processing for an entire Spring application context.
 * <p>
 * JavaBean that allows for configuring a ThreadPoolExecutor in bean style
 *
 * @see ThreadPoolTaskExecutor
 * @see ThreadPoolExecutor
 * @see Executor
 */
@Configuration
@EnableAsync
public class AsyncConfiguration
{
    @Bean(name = "asyncExecutor")
    public Executor asyncExecutor()
    {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(16);
        executor.setMaxPoolSize(32);
        executor.setThreadNamePrefix("AsyncThread-");
        executor.initialize();
        return executor;
    }
}
