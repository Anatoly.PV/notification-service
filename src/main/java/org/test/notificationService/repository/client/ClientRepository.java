package org.test.notificationService.repository.client;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.test.notificationService.domain.entity.ClientEntity;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;


/**
 * JpaRepository interface declared persistence methods for client
 */
@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, UUID> {

    @Async("asyncExecutor")
    @Query(value = "SELECT * from client where delete_flag=false and phone_number=:phoneNumber", nativeQuery = true)
    CompletableFuture<List<ClientEntity>> getByPhoneNumberAsync(String phoneNumber);

    @Async("asyncExecutor")
    @Query(value = "SELECT * from client  where delete_flag=false and time_zone=:timeZone", nativeQuery = true)
    CompletableFuture<List<ClientEntity>> getByTimeZoneAsync(String timeZone);

    @Async("asyncExecutor")
    @Query(value = "SELECT * from client  where delete_flag=false and tag=:tag", nativeQuery = true)
    CompletableFuture<List<ClientEntity>> getByTagAsync(String tag);

    @Async("asyncExecutor")
    @Query(value = "SELECT * from client where delete_flag=false and code=:code", nativeQuery = true)
    CompletableFuture<List<ClientEntity>> getByCodeAsync(String code);

}