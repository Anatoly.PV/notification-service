package org.test.notificationService.repository.mailing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.test.notificationService.domain.entity.MailingEntity;

import java.util.UUID;

/**
 * JpaRepository interface declared persistence methods for mailing
 */
@Repository
public interface MailingRepository extends JpaRepository<MailingEntity, UUID> {


}