package org.test.notificationService.repository.message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.test.notificationService.domain.entity.MailingEntity;
import org.test.notificationService.domain.entity.MessageEntity;
import org.test.notificationService.domain.enumType.MessageStatus;

import java.util.List;

/**
 * JpaRepository interface declared persistence methods for message
 */
@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, Integer> {

    @Query(value = "select * from message where delete_flag=false", nativeQuery = true)
    List<MessageEntity> getAll();

    List<MessageEntity> findAllByMailingAndCheckStatus(MailingEntity mailing, MessageStatus status);
}
