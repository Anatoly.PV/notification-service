package org.test.notificationService.constant;

import org.test.notificationService.domain.entity.MailingEntity;
import org.test.notificationService.service.camel.MessageCheckout;
import org.test.notificationService.service.camel.MessageExecutorService;
import org.test.notificationService.service.camel.MessageStorage;

/**
 * This stores global parameter's witch using inside Camel router
 * SQL_QUERY - this query return {@link MailingEntity} with has READY or REJECTED status
 * DESTINATION - named header for camel routers
 * IS_EXECUTED - set header as Executor {@link MessageExecutorService}
 * IS_CHECKOUT - set header as checkout {@link MessageCheckout}
 * IS_SAVED - set header as saved {@link MessageStorage}
 */
public class CamelHelper {

    public static final String SQL_QUERY = "SELECT " +
            "message.id, " +
            "message.create_date, " +
            "message.send_date, " +
            "message_status, " +
            "message.delete_flag, " +
            "mailing_id, " +
            "client_id, " +
            "mailing.start_date, " +
            "mailing.end_date " +
            "FROM message " +
            "inner join mailing on  message.mailing_id = mailing.id " +
            "WHERE message.delete_flag = false and message.message_status = 'READY' " +
            "or message.message_status = 'REJECTED'";

    public static final String DESTINATION = "destination";

    public static final String IS_EXECUTED = " is executed";

    public static final String IS_CHECKOUT = " is checkout";

    public static final String IS_SAVED = " is saved";

}