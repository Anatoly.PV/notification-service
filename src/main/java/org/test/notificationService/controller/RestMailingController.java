package org.test.notificationService.controller;

import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.test.notificationService.domain.dto.mailing.MailingDto;
import org.test.notificationService.domain.dto.mailing.ResultMailingDto;
import org.test.notificationService.domain.dto.message.MessageMailingDetails;
import org.test.notificationService.domain.enumType.MessageStatus;
import org.test.notificationService.service.MailingService;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * The class contains endpoints that process requests to parse data input for mailing.
 * Only route to service {@link MailingService}
 * <p>
 * Path: url/mailing/method name
 * supports GET,POST,PATCH,DELETE methods
 *
 * @see RequestMapping
 * @see RestController
 * @see MailingService
 */
@RestController()
@RequestMapping("/mailing")
public class RestMailingController {

    @Resource
    private MailingService mailingService;

    /**
     * Get mailings method
     *
     * @return all mailings {@link ResultMailingDto}
     */
    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<ResultMailingDto>> getAll() {

        return ResponseEntity.ok().body(mailingService.getAll());
    }

    /**
     * Create mailing method
     *
     * @param createMailingData {@link MailingDto}
     * @return saved result {@link ResultMailingDto}
     */
    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public ResponseEntity<ResultMailingDto> create(@RequestBody MailingDto createMailingData) {

        return ResponseEntity.ok().body(mailingService.create(createMailingData));
    }

    /**
     * Update mailing method
     *
     * @param id                {@link UUID}
     * @param updateRequestData {@link MailingDto}
     * @return updated result {@link ResultMailingDto}
     */
    @RequestMapping(path = "/update/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<ResultMailingDto> update(
            @PathVariable @Parameter(description = "Уникальный идентификатор (ID) рассылки", required = true, example = "90692ca1-e2fa-44b5-a172-3f1719ce8481") UUID id,
            @RequestBody MailingDto updateRequestData) {

        return ResponseEntity.ok().body(mailingService.update(id, updateRequestData));
    }

    /**
     * Get mailing method
     *
     * @param id {@link UUID}
     * @return get result {@link ResultMailingDto}
     */
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ResultMailingDto> getById(
            @PathVariable @Parameter(description = "Уникальный идентификатор (ID) рассылки", required = true, example = "90692ca1-e2fa-44b5-a172-3f1719ce8481") UUID id) {

        return ResponseEntity.ok().body(mailingService.getById(id));
    }

    /**
     * Filter mailing method by id and status
     *
     * @param id     {@link UUID}
     * @param status {@link MessageStatus}
     * @return filtering result {@link MessageMailingDetails}
     */
    @RequestMapping(path = "/{id}/{status}", method = RequestMethod.GET)
    public ResponseEntity<List<MessageMailingDetails>> getAllByStatus(
            @PathVariable @Parameter(description = "Уникальный идентификатор (ID) рассылки", required = true, example = "90692ca1-e2fa-44b5-a172-3f1719ce8481") UUID id,
            @PathVariable("status") @Parameter(description = "Статус сообщения рассылки") MessageStatus status) {

        return ResponseEntity.ok().body(mailingService.getAllByStatus(id, status));
    }

    /**
     * Delete mailing method
     *
     * @param id     {@link UUID}
     * @return deleted result {@link Boolean}
     */
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(
            @PathVariable @Parameter(description = "Уникальный идентификатор (ID) рассылки", required = true, example = "90692ca1-e2fa-44b5-a172-3f1719ce8481") UUID id) {

        return ResponseEntity.ok().body(mailingService.softDelete(id));
    }
}
