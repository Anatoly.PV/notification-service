package org.test.notificationService.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.test.notificationService.domain.dto.message.MessageInfoResultDto;
import org.test.notificationService.service.MessageService;

import javax.annotation.Resource;
import java.util.List;

/**
 * The class contains endpoints that process requests to parse data input for message.
 * Only route to service {@link MessageService}
 * <p>
 * Path: url/message/method name
 * only supports GET method!
 *
 * @see RequestMapping
 * @see RestController
 * @see MessageService
 */
@RestController()
@RequestMapping("/message")
public class RestMessageController {

    @Resource
    private MessageService messageService;

    /**
     * Get message method
     *
     * @return all messages {@link MessageInfoResultDto}
     */
    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<MessageInfoResultDto>> getTransactionsWithAccountAndPeriod(){

        return ResponseEntity.ok().body(messageService.getAll());

    }

}