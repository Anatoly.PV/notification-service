package org.test.notificationService.controller;

import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.test.notificationService.domain.dto.client.ClientDto;
import org.test.notificationService.domain.dto.client.ResultClientDto;
import org.test.notificationService.service.ClientService;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * The class contains endpoints that process requests to parse data input for client.
 * Only route to service {@link ClientService}
 * <p>
 * Path: url/client/method name
 * supports GET,POST,PATCH,DELETE methods
 *
 * @see RequestMapping
 * @see RestController
 * @see ClientService
 */
@RestController
@RequestMapping("/client")
public class RestClientController {

    @Resource
    private ClientService clientService;

    /**
     * Get clients method
     *
     * @return all clients {@link ResultClientDto}
     */
    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<ResultClientDto>> getAll() {

        return ResponseEntity.ok().body(clientService.getAll());

    }

    /**
     * Create client method
     *
     * @param createRequestData {@link ClientDto}
     * @return saved result {@link ResultClientDto}
     */
    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public ResponseEntity<ResultClientDto> create(@RequestBody ClientDto createRequestData) {

        return ResponseEntity.ok().body(clientService.create(createRequestData));

    }

    /**
     * Update client method
     *
     * @param id                {@link UUID}
     * @param updateRequestData {@link ClientDto}
     * @return updated result {@link ResultClientDto}
     */
    @RequestMapping(path = "/update/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<ResultClientDto> update(
            @PathVariable @Parameter(description = "Уникальный идентификатор (ID) клиента", required = true, example = "90692ca1-e2fa-44b5-a172-3f1719ce8481") UUID id,
            @RequestBody ClientDto updateRequestData) {

        return ResponseEntity.ok().body(clientService.update(id, updateRequestData));

    }

    /**
     * Delete client method
     *
     * @param id {@link UUID}
     * @return deleted result {@link Boolean}
     */
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(
            @PathVariable @Parameter(description = "Уникальный идентификатор (ID) клиента", required = true, example = "90692ca1-e2fa-44b5-a172-3f1719ce8481") UUID id) {

        return ResponseEntity.ok().body(clientService.softDelete(id));

    }
}