CREATE TYPE message_status_enum AS ENUM ( 'SUCCESS','WAITING','EXPIRED','REJECTED','READY');

create table if not exists mailing
(
    id          uuid         not null
        constraint mailing_pkey primary key,
    content     varchar(255) not null,
    start_date  timestamptz,
    end_date    timestamptz,
    delete_flag bool         not null
);

create table if not exists client
(
    id           uuid         not null
        constraint client_pkey primary key,
    phone_number varchar(255) not null,
    code         varchar(255) not null,
    tag          varchar(255) not null,
    time_zone    varchar(255) not null,
    delete_flag  bool         not null
);

create table if not exists message
(
    id serial constraint message_pkey primary key,
    create_date     timestamptz,
    send_date     timestamptz,
    message_status message_status_enum,
    delete_flag    bool   not null,
    mailing_id uuid
        constraint mailing_pkey references mailing,
    client_id  uuid
        constraint client_pkey references client
);

create table if not exists mailing_to_client
(
    mailing_id uuid
        constraint mailing_pkey references mailing,
    client_id  uuid
        constraint client_pkey references client,
    constraint mailing_to_client_pkey
        primary key (mailing_id, client_id)
);


















