package org.test.notificationService;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.test.notificationService.behavior.Action;
import org.test.notificationService.behavior.PostCondition;
import org.test.notificationService.behavior.PreCondition;
import org.test.notificationService.configuration.TestContentParserConfiguration;
import org.test.notificationService.domain.dto.client.ClientDto;
import org.test.notificationService.domain.dto.client.ResultClientDto;
import org.test.notificationService.domain.dto.message.MessageMailingDetails;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.UUID;

@TestPropertySource(locations = "classpath:application.yml")
@Import({TestContentParserConfiguration.class, PreCondition.class, PostCondition.class, Action.class})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {NotificationServiceApplication.class})
public class IntegrationNotificationServiceTest extends AbstractTestNGSpringContextTests {

    @Resource
    private PreCondition preCondition;

    @Resource
    private PostCondition postCondition;

    @Resource
    private Action action;

    @BeforeClass
    public void main() {
        NotificationServiceApplication.main(new String[] {});
    }

    /**
     * validate response result
     */
    @Test
    public void createClient() {

        ClientDto requestBody  = preCondition.getCreateClientWithRandomNumberRequestBody("Europe/Moscow");

        ResponseEntity<ResultClientDto> result = action.restCreateClientExecute(requestBody);

        postCondition.validateStatus(result);

    }
}
