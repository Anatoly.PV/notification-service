package org.test.notificationService.behavior;

import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.test.notificationService.domain.dto.client.ResultClientDto;

@TestComponent
public class PostCondition {


    public void validateStatus(ResponseEntity<ResultClientDto> result) {
        Assert.isTrue(result.getStatusCode() == HttpStatus.OK,result.getStatusCode().toString());
    }
}
