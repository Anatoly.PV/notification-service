package org.test.notificationService.behavior;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.test.notificationService.domain.dto.client.ClientDto;
import org.test.notificationService.domain.dto.client.ResultClientDto;
import org.testng.Assert;

import javax.annotation.Resource;

@TestComponent
public class Action {

    @Value("${test.restCreateClientUrl}")
    private String requestUri;

    @Resource(name = "testRestTemplate")
    private RestTemplate restTemplate;

    public ResponseEntity<ResultClientDto> restCreateClientExecute(ClientDto requestBody) {

        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(requestUri);

            return restTemplate.postForEntity(builder.toUriString(), requestBody, ResultClientDto.class);

        } catch (Exception ex) {
            Assert.assertThrows(Exception.class,
                    () -> System.out.println(ex.getMessage()));
            throw ex;
        }

    }

}