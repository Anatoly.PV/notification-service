package org.test.notificationService.behavior;

import org.springframework.boot.test.context.TestComponent;
import org.test.notificationService.domain.dto.client.ClientDto;

import java.util.Random;

@TestComponent
public class PreCondition {

    public ClientDto getCreateClientWithRandomNumberRequestBody(String timeZone) {

        int value = (int) Math.pow(10, 9);
        String valueString = String.valueOf(value + new Random().nextInt(9 * value));
        String code = valueString.substring(0,3);
        String number = "7" + valueString;

        return ClientDto.builder()
                .phoneNumber(number).code(code).tag("тест").timeZone(timeZone)
                .build();
    }
}
